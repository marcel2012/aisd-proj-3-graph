#include <stdio.h>
#include <assert.h>

#define get(graph, n, i, j) (graph[i*n+j]-'0')
#define isOddCycle(n, minD, maxD) (n%2 && minD==2 && maxD==2)
#define isFull(n, minD, maxD) (minD==(n-1) && maxD==(n-1))

inline void dfs(char *graph, const int &size, int &n, int &minD, int &maxD, bool *visited, const int &k) {
    if (!visited[k]) {
        visited[k] = true;
        n++;
        int d = 0;
        for (int i = 0; i < size; i++)
            if (get(graph, size, k, i)) {
                d++;
                dfs(graph, size, n, minD, maxD, visited, i);
            }
        if (d > maxD)
            maxD = d;
        if (d < minD)
            minD = d;
    }
}

inline bool check(char *graph, const int &size) {
    bool *visited = new bool[size]();
    int n, minD, maxD;
    int maxDegreeInAllParts = 0;
    bool resultForMaxDegree = false;
    for (int i = 0; i < size; i++)
        if (!visited[i]) {
            n = maxD = 0;
            minD = size;
            dfs(graph, size, n, minD, maxD, visited, i);
            if (maxD > maxDegreeInAllParts) {
                maxDegreeInAllParts = maxD;
                resultForMaxDegree = isOddCycle(n, minD, maxD) || isFull(n, minD, maxD);
            } else if (maxD == maxDegreeInAllParts && !resultForMaxDegree)
                resultForMaxDegree = isOddCycle(n, minD, maxD) || isFull(n, minD, maxD);
        }
    delete[] visited;
    return resultForMaxDegree;
}

int main() {
    assert(check("0111110111110111110111110", 5) == true);
    assert(check("0110010000100110010000100", 5) == false);
    assert(check("0110111101000111011110010100101101010101011110010", 7) == false);
    assert(check("010001101000010100001010000101100010", 6) == false);
    assert(check("0111101011001000", 4) == false);
    assert(check("011111101010110101101011110101101110", 6) == false);
    assert(check("0111111101111111011111110111111101111111011111110", 7) == true);
    assert(check("010010101100010000010000100001000010", 6) == false);
    assert(check("0110111101111111011110110111111101111111011111110", 7) == false);
    assert(check("011101110", 3) == true);
    assert(check("0110", 2) == true);
    assert(check("0111101011001000", 4) == false);
    assert(check("0111101111011110", 4) == true);
    assert(check("0101101001011010", 4) == false);
    assert(check("0110", 2) == true);
    assert(check("010001101000010100001010000101100010", 6) == false);
    assert(check("011101110", 3) == true);
    return 0;

    int p, n;
    scanf("%d", &p);
    while (p--) {
        scanf("%d ", &n);
        char *tab = new char[n * n];
        scanf("%s", tab);
        if (check(tab, n))
            printf("True\n");
        else
            printf("False\n");
        delete[] tab;
    }
    return 0;
}